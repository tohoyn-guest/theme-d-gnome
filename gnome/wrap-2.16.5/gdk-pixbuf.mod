
(source "common-explicit.defs")

(import-external (standard-library core)
		 (gnome gobject)
		 (gnome common)
		 (gnome wrap gdk-pixbuf-support))

;; gdk-pixbuf-save-to-port can't be used as a method name since it is
;; already used as a simple procedure name.
(no-method "gdk_pixbuf_save_to_port" "GdkPixbuf")

(generate-header)

(source "defs/gdk-pixbuf.defs")
