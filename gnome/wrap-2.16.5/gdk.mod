;; -*-scheme-*-

(source "common-explicit.defs")

(import-external (standard-library core))
(import-external (gnome gobject))
(import-external (gnome common))
(import-external (gnome wrap gdk-support))
(import-external (gnome wrap pango-support))

(import gdk-pixbuf)
(import pango)

(no-method "copy" "GdkColor")
(no-method "copy" "GdkEvent")
(no-method "copy" "GdkRegion")
(no-method "equal" "GdkRegion")

;; "xor" and "raise" cannot be used as generic procedure names
;; since they are already defined in the core library.
(no-method "xor" "GdkRegion")
(no-method "raise" "GdkWindow")

;; Classes <pango-attr-stipple> and <pango-attr-embossed> are not
;; defined in guile-gnome.
(no-creator "gdk_pango_attr_stipple_new")
(no-creator "gdk_pango_attr_embossed_new")

(generate-header)

(source "defs/gdk.defs")
