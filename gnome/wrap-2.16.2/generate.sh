#!/bin/sh

modules="atk pango gdk-pixbuf gdk gtk"
export OVERRIDES=/usr/share/guile-gnome-2/gnome/overrides
export THEME_D_GNOME_SOURCE_PATH=.:/usr/share/guile-gnome-2:/usr/share/guile-gnome-2/gnome

for module in $modules ; do
    echo "Generating interface for module " $module ;
    run-theme-d-program ../../theme-d-gen/generate-theme-d-code.go \
	$module $module.thi interface "gnome wrap" $OVERRIDES ;
    echo "Generating body for module " $module ;
    run-theme-d-program ../../theme-d-gen/generate-theme-d-code.go \
	$module $module.thb body "gnome wrap" $OVERRIDES ;
done
