
(source "common-explicit.defs")

(import-external (standard-library core)
		 (gnome gobject)
		 (gnome common)
		 (gnome wrap gdk-pixbuf-support))

(no-method "gdk_pixbuf_save_to_port" "GdkPixbuf")

(generate-header)

(source "defs/gdk-pixbuf.defs")
