;; -*-scheme-*-

(source "common-explicit.defs")

(import-external (standard-library core))
(import-external (gnome gobject)
		 (gnome common)
		 (gnome wrap pango-support))

;; "splice" is a keyword in Theme-D so we can't define a generic procedure
;; having that name.
(no-method "splice" "PangoAttrList")
;; "to-string" is already defined in the Theme-D standard library
(no-method "to_string" "PangoFontDescription")
(no-method "copy" "PangoAttribute")
(no-method "copy" "PangoAttrIterator")
(no-method "copy" "PangoCoverage")
(no-method "next" "PangoAttrIterator")
(no-method "next" "PangoScriptIter")
(no-method "equal" "PangoAttribute")

(no-creator "pango_attr_language_new")
(no-creator "pango_attr_family_new")
(no-creator "pango_attr_foreground_new")
(no-creator "pango_attr_background_new")
(no-creator "pango_attr_size_new")
(no-creator "pango_attr_style_new")
(no-creator "pango_attr_weight_new")
(no-creator "pango_attr_variant_new")
(no-creator "pango_attr_stretch_new")
(no-creator "pango_attr_font_desc_new")
(no-creator "pango_attr_underline_new")
(no-creator "pango_attr_underline_color_new")
(no-creator "pango_attr_strikethrough_new")
(no-creator "pango_attr_strikethrough_color_new")
(no-creator "pango_attr_rise_new")
(no-creator "pango_attr_scale_new")
(no-creator "pango_attr_fallback_new")
(no-creator "pango_attr_letter_spacing_new")
(no-creator "pango_attr_shape_new")

(generate-header)

(source "defs/pango.defs")

