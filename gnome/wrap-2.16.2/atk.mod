;; -*-scheme-*-

(source "common-explicit.defs")

(import-external (standard-library core))
(import-external (gnome gobject)
		 (gnome common)
		 (gnome wrap atk-support))

(generate-header)

(source "defs/atk.defs")
