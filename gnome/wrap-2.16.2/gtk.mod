;; -*-scheme-*-

(source "common-explicit.defs")

(import-external (standard-library core))
(import-external (gnome gobject))
(import-external (gnome common))
(import-external (gnome wrap gdk-support))
(import-external (gnome wrap gdk-pixbuf-support))
(import-external (gnome wrap pango-support))
(import-external (gnome wrap gtk-support))
(import-external (gnome wrap atk-support))

(import gdk-pixbuf)
(import gdk)
(import pango)
(import atk)

;; "append" and "map" cannot be used as a generic procedure names
;; since they have already been defined in the core library.
(no-method "append" "GtkListStore")
(no-method "append" "GtkTreeStore")
(no-method "append" "GtkMenuShell")
(no-method "map" "GtkWidget")
(no-method "activate" "GtkMenuItem")
(no-method "get_style" "GtkToolbar")

;; "gtk_rc_style_new" has erroneous class in guile-gnome.
(no-creator "gtk_rc_style_new")

(generate-header)

(source "gtk-my-defs.defs")
(source "defs/gtk.defs")
