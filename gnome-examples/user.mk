
COMP_OPTIONS := $(EXTRA_COMP_OPTIONS)
LINK_OPTIONS := $(EXTRA_LINK_OPTIONS)

ifdef PRETTY_PRINT
	COMP_OPTIONS += --pretty-print
	LINK_OPTIONS += --pretty-print
endif

ifdef NO_LINK_OPTIMIZATION
	LINK_OPTIONS += --no-factorization --no-strip
endif

PROGRAMS = hello calc

TARGETS = $(patsubst %,%.go,$(PROGRAMS))
INT_TARGETS = $(patsubst %,%.tree-il,$(PROGRAMS))

TDC := theme-d-compile

ifdef LOCAL_MODE
	TDL := ../scripts/theme-d-gnome-link
else
	TDL := theme-d-gnome-link
endif

ifdef LOCAL_MODE
	GUILD := ../scripts/guild-gnome-2
else
	GUILD := guild-gnome-2
endif

.PHONY : all clean

all : $(TARGETS)

clean :
	-rm -f $(TARGETS) $(INT_TARGETS) $(patsubst %,%.tcp,$(PROGRAMS))

$(INT_TARGETS) : %.tree-il : %.tcp
	$(TDL) $(LINK_OPTIONS) -m ..: \
	  --no-final-compilation --keep-intermediate -n $@ $<

$(TARGETS) : %.go : %.tree-il
	$(GUILD) compile --from=tree-il -o $@ $<

%.tcp : %.thp
	$(TDC) $(COMP_OPTIONS) -m ..: -o $@ $<

# %.tci : %.thi
# 	$(TDC) -o $@ $<

# %.tcb : %.thb %.tci
# 	$(TDC) -o $@ $<
