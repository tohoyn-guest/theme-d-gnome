
COMP_OPTIONS := $(EXTRA_COMP_OPTIONS)
LINK_OPTIONS := $(EXTRA_LINK_OPTIONS)

ifdef PRETTY_PRINT
	COMP_OPTIONS += --pretty-print
	LINK_OPTIONS += --pretty-print
endif

ifdef NO_LINK_OPTIMIZATION
	LINK_OPTIONS += --no-factorization --no-strip
endif

PROGRAMS = run-demo

TARGETS = $(patsubst %,%.go,$(PROGRAMS))
INT_TARGETS = $(patsubst %,%.tree-il,$(PROGRAMS))

TDC := theme-d-compile

ifdef LOCAL_MODE
	TDL := ../../scripts/theme-d-gnome-link
else
	TDL := theme-d-gnome-link
endif

ifdef LOCAL_MODE
	GUILD := ../../scripts/guild-gnome-2
else
	GUILD := guild-gnome-2
endif

.PHONY : all clean

all : $(TARGETS)

clean :
	-rm -f $(TARGETS) $(INT_TARGETS) $(patsubst %,%.tcp,$(PROGRAMS))
	-rm -f *.tci
	-rm -f *.tcb
	-rm -f *.tcp

$(INT_TARGETS) : %.tree-il : %.tcp
	$(TDL) $(LINK_OPTIONS) -m ../..: \
	  --no-final-compilation --keep-intermediate -n $@ $<

$(TARGETS) : %.go : %.tree-il
	$(GUILD) compile --from=tree-il -o $@ $<

%.tcp : %.thp
	$(TDC) $(COMP_OPTIONS) -m ../..: -o $@ $<

%.tci : %.thi
	$(TDC) $(COMP_OPTIONS) -m ../..: -o $@ $<

%.tcb : %.thb %.tci
	$(TDC) $(COMP_OPTIONS) -m ../..: -o $@ $<

run-demo.tree-il : demo.tcb \
	dialog.tcb panes.tcb sizegroup.tcb button-box.tcb \
	list-store.tcb tree-store.tcb \
	appwindow.tcb \
	support.tcb

run-demo.tcp : demo.tci \
	dialog.tci panes.tci sizegroup.tci button-box.tci \
	list-store.tci tree-store.tci \
	appwindow.tci \
	support.tci

dialog.tci : demo.tci

dialog.tcb : demo.tci support.tci

panes.tci : demo.tci

panes.tcb : demo.tci support.tci

sizegroup.tci : demo.tci

sizegroup.tcb : demo.tci support.tci

button-box.tci : demo.tci

button-box.tcb : demo.tci support.tci

list-store.tci : demo.tci

list-store.tcb : demo.tci support.tci

tree-store.tci : demo.tci

tree-store.tcb : demo.tci support.tci

appwindow.tci : demo.tci

appwindow.tcb : demo.tci support.tci
