
\documentclass[a4paper]{article}
\usepackage{amsmath,amssymb,amsthm}
\usepackage[utf8]{inputenc}


\newcommand{\themedgnome}{Theme-D-Gnome}
\newcommand{\themed}{Theme-D}
\newcommand{\themedver}{4.0.0}
\newcommand{\themedgnomeupstreamver}{0.9.6}
\newcommand{\debianrev}{{\rmfamily\textit{rev}}}
\newcommand{\themedgnomever}{\themedgnomeupstreamver-\debianrev}
\newcommand{\guilegnome}{guile-gnome-2}
\newcommand{\arch}{all}


\input commands.tex


\begin{document}


\begin{titlepage}
\title{\themedgnome\ User Guide}
\author{Tommi H\"oyn\"al\"anmaa}
\maketitle
\end{titlepage}


\tableofcontents


\section{Copyright}

Copyright (C) 2015-2021 Tommi Höynälänmaa

\vspace{0.5cm}

Contents of subdirectories \code{theme-d-gen} and
\code{gnome-examples} are licensed under GNU General Public
License. See file \code{doc/GPL-3}.  The contents of subdirectories
\code{gnome} and \code{theme-d-gnome/runtime} are licensed under GNU
Lesser General Public License. See file \code{doc/LGPL-3}. All
documentation (subdirectory \code{doc}) is licensed under the GNU Free
Documentation License. See file \code{doc/GFDL-1.3}.


\section{General}

This guide covers only UNIX systems. Many commands in this guide have
to be executed in a root session. A root session is launched with
command \code{su root} or \code{sudo} depending on your system. In
Ubuntu the command is \code{sudo}.
Symbol \debianrev\ in the package names means the Debian revision of
the packages. It is typically 1.


\section{Software needed}

You need at least the following software to use \themedgnome:
\begin{itemize}
\item guile version 2.2
\item \guilegnome\ version 2.16.2, 2.16.4, or 2.16.5
\item GNU make
\item Theme-D
\end{itemize}

The proper Theme-D version depends on the version of Theme-D-Gnome you
use. Theme-D-Gnome version 0.9.6-1 requires Theme-D version 4.0.0-1.
When a new Theme-D version is released a new Theme-D-Gnome version is
released if necessary.

TH Scheme Utilities and Theme-D can be obtained from

\vspace{0.5cm}
\noindent
\mbox{ } \hspace{1.0cm} \myurl{http://www.iki.fi/tohoyn/theme-d/}.
\vspace{0.5cm}

Guile can be obtained from

\vspace{0.5cm}
\noindent
\mbox{ } \hspace{1.0cm} \myurl{http://www.gnu.org/software/guile/}.
\vspace{0.5cm}

\guilegnome\ can be obtained from

\vspace{0.5cm}
\noindent
\mbox{ } \hspace{1.0cm} \myurl{http://www.gnu.org/software/guile-gnome/}.
\vspace{0.5cm}

If you use Debian or Ubuntu Linux it is recommended that you install
guile 2.2 with commands

\vspace{0.5cm}
\noindent
\mbox{ } \hspace{1.0cm} \code{apt-get install guile-2.2}
\\
\noindent
\mbox{ } \hspace{1.0cm} \code{apt-get install guile-2.2-dev}
\vspace{0.5cm}

and \guilegnome\ with commands

\vspace{0.5cm}
\noindent
\mbox{ } \hspace{1.0cm} \code{apt-get install
  guile-gnome2-dev} \\
\mbox{ } \hspace{1.0cm} \code{apt-get install
  guile-gnome2-glib} \\
\mbox{ } \hspace{1.0cm} \code{apt-get install
  guile-gnome2-gtk}
\vspace{0.5cm}

All of these commands have to be executed in a root session.
Note that \themedgnome\ may not work for \guilegnome\ versions other
than 2.16.2, 2.16.4, or 2.16.5. If you use such versions follow the
instructions in section \ref{sec:regeneration} to regenerate the
wrapper libraries.


\section{Installation}


You can check the version number of your \guilegnome\ library
with command
\begin{codeblock}
  \noindent
  \mbox{ } \hspace{0.5cm} pkg-config --modversion guile-gnome-gtk-2
\end{codeblock}


\subsection{Debian-based systems}


In particular these instructions apply to Debian and Ubuntu Linux
distributions. If you have installed Theme-D with command
\code{apt-get install} you should install Theme-D-Gnome with command
\begin{codeblock}
  \noindent
  \mbox{ } \hspace{0.5cm}
  sudo apt-get install theme-d-gnome theme-d-gnome-dev
\end{codeblock}
\noindent
Otherwise install the software with commands
\begin{codeblock}
  \noindent
  \mbox{ } \hspace{0.5cm}
  sudo dpkg -i theme-d-gnome\_\themedgnomever\_\arch.deb \\
  \noindent
  \mbox{ } \hspace{0.5cm}
  sudo dpkg -i theme-d-gnome-dev\_\themedgnomever\_\arch.deb
\end{codeblock}


\subsection{Rebuilding the Debian Packages}
\label{sec:other-arch}


\begin{enumerate}
\item Create a new directory for building \themedgnome and change into
  that directory. This directory shall be called the
  \textit{build directory}.
\item
  Copy files \code{theme-d-gnome-\themedgnomeupstreamver.tar.xz}
  and \code{theme-d-gnome\_\themedgnomever.debian.tar.xz} into the
  build directory.
\item
  Give command
  \begin{codeblock}
    \noindent
    \mbox{ } \hspace{0.5cm}
    ln -s theme-d-gnome-\themedgnomeupstreamver.tar.xz \
    theme-d-gnome\_\themedgnomeupstreamver.orig.tar.xz
  \end{codeblock}
\item
  Unpack the source code with command
  \begin{codeblock}
    \noindent
    \mbox{ } \hspace{0.5cm}
    tar xvf theme-d-gnome-\themedgnomeupstreamver.tar.xz
  \end{codeblock}
\item
  Give commands
  \begin{codeblock}
    \noindent
    \mbox{ } \hspace{0.5cm}
    cd theme-d-gnome-\themedgnomeupstreamver \\
    \noindent
    \mbox{ } \hspace{0.5cm}
    tar xvf ../theme-d-gnome\_\themedgnomever.debian.tar.xz
  \end{codeblock}
\item If you use \guilegnome\ version 2.16.2 or 2.16.4 change the
  value of variable \code{GUILE\_GNOME\_VERSION} in file
  \code{debian/rules} (line 5).
\item
  Build the Debian packages with command
  \begin{codeblock}
    \noindent
    \mbox{ } \hspace{0.5cm}
    dpkg-buildpackage -uc -us -b
  \end{codeblock}
  in subdirectory \code{theme-d-gnome-\themedgnomeupstreamver}.
\item Change to the parent directory and install the generated Debian package
  with commands
  \begin{codeblock}
    \noindent
    \mbox{ } \hspace{0.5cm} dpkg -i
    theme-d-gnome\_\themedgnomever\_\arch.deb \\
    \noindent
    \mbox{ } \hspace{0.5cm} dpkg -i
    theme-d-gnome-dev\_\themedgnomever\_\arch.deb
  \end{codeblock}
  The commands have to be run as root.
\end{enumerate}


\subsection{Other systems}


\begin{enumerate}
\item
  Unpack the source package
  \code{theme-d-gnome-\themedgnomeupstreamver.tar.xz}
  into some directory.
\item
  Change to the subdirectory
  \code{theme-d-gnome-\themedgnomeupstreamver} and give command
  \begin{codeblock}
    \noindent
    \mbox{ } \hspace{0.5cm} ./configure
  \end{codeblock}
  In case you have \guilegnome\ version 2.16.2 use command
  \begin{codeblock}
    \noindent
    \mbox{ } \hspace{0.5cm} ./configure --with-guile-gnome=2.16.2
  \end{codeblock}
  instead.
  In case you have \guilegnome\ version 2.16.4 use command
  \begin{codeblock}
    \noindent
    \mbox{ } \hspace{0.5cm} ./configure --with-guile-gnome=2.16.4
  \end{codeblock}
\item
  Give command
  \begin{codeblock}
    \noindent
    \mbox{ } \hspace{0.5cm} make
  \end{codeblock}
\item
  Install \themedgnome\ with command
  \begin{codeblock}
    \noindent
    \mbox{ } \hspace{0.5cm} make install-complete
  \end{codeblock}
  in a root session.
\end{enumerate}


\subsection{Using the Software without Installation}


It is possible to use \themedgnome\ without installing it. This is
called \textit{local mode}. In order to do this unpack the source code
package \code{theme-d-gnome-\themedgnomeupstreamver.tar.xz} into
some directory and execute commands
\begin{codeblock}
  \noindent
  \mbox{ } \hspace{0.5cm} ./configure \\
  \noindent
  \mbox{ } \hspace{0.5cm} make
\end{codeblock}
in subdirectory \code{theme-d-gnome-\themedgnomeupstreamver}.
Now you can use the software by changing to subdirectory \code{meta}
and giving command
\begin{codeblock}
  \noindent
  \mbox{ } \hspace{0.5cm} ./uninstalled-env bash
\end{codeblock}


\section{Removing the software}


\subsection{Debian-based systems}


If you have installed the software with command \code{apt-get} give
command
\begin{codeblock}
  \noindent
  \mbox{ } \hspace{0.5cm} apt-get remove theme-d-gnome theme-d-gnome-dev
\end{codeblock}
\noindent
in a root session.
If you have installed the software with command \code{dpkg} give
command
\begin{codeblock}
  \noindent
  \mbox{ } \hspace{0.5cm} dpkg --purge theme-d-gnome theme-d-gnome-dev
\end{codeblock}
\noindent
in a root session.


\subsection{Other systems}


Give command
\begin{codeblock}
  \noindent
  \mbox{ } \hspace{0.5cm} make uninstall-complete
\end{codeblock}
in subdirectory \code{theme-d-gnome-\themedgnomeupstreamver} of the
directory where you unpacked \themedgnome. The command has to be run
in a root session.


\section{Running the example programs}

Copy the example programs and makefiles (\code{user.mk}) into the
directory where you want to build them.  The examples programs can be
found in directory \code{/usr/share/doc/theme-d-gnome/examples} or in
subdirectory \code{gnome-examples} in the \themedgnome\ source
package.  Note that the example programs must be contained in
subdirectory \code{gnome-examples} in the target directory.  Give
command \code{make -f user.mk XXX.go} where \code{XXX} is the name of
the program (\code{hello} or \code{calc}).  In order to run an
application give command \code{rtp-gnome XXX.go}.  You can also
compile both the examples by command \code{make -f user.mk} in
subdirectory \code{gnome-examples}.  In order to run the
\themedgnome\ demo build the program with command \code{make -f
  user.mk} and run it with command
\begin{codeblock}
  \noindent
  \mbox{ } \hspace{0.5cm} rtp-gnome run-demo.go
\end{codeblock}
\noindent
in subdirectory \code{gnome-examples/theme-d-gnome-demo}.
Note that you may have add directory \code{/usr/share/guile-gnome-2}
into environment variable \code{GUILE\_LOAD\_PATH} to be able to run
programs using the software.


\section{Making programs that use \themedgnome}

When you make your own programs using \themedgnome\ you can compile
the programs in the same way as other \themed\ programs using command
\code{theme-d-compile} but you have to link the programs with command
\code{theme-d-gnome-link}.  This ensures that the guile-gnome
libraries are loaded by the target programs.  When you run your own
programs using \themedgnome\ you have to ensure that necessary GNOME
modules are linked into guile. This can be done by executing guile
with command \code{\guilegnome}.  Easiest way to fulfill these
requirements is to use command
\begin{codeblock}
  rtp-gnome MYPROGRAM.go
\end{codeblock}
\noindent
to run your programs.  The program \code{rtp-gnome} accepts the same
options \code{--no-verbose-errors}, \code{--backtrace}, and
\code{--pretty-backtrace} as program \code{run-theme-d-program}, see
the \themed\ User Guide.


\section{Distributing programs that use \themedgnome}

If you distribute your \themedgnome\ program to a Debian system it is
easiest to install the following packages in the target system:
\begin{itemize}
\item \code{theme-d-rte}
\item \code{libthemedsupport}
\item \code{th-scheme-utilities}
\item \code{theme-d-gnome}
\end{itemize}
Note that you don't need package \code{theme-d-gnome-dev} if you just
want to run a program using \themedgnome.

In case you distribute a \themedgnome\ program into a non-Debian system
see section 11 in the \themed\ User Guide for further information. You
also need to install file
\code{theme-d-code/runtime/gnome-support.scm}
into the subdirectory \code{theme-d-code/runtime} of the Guile site
directory. You can find out this directory with command
\begin{codeblock}
  pkg-config --variable=sitedir guile-{\rmfamily\textit{guile-version}}
\end{codeblock}
You need to install file
\code{theme-d-code/runtime/rtp-gnome.scm} into the target system in
whatever directory where you want to invoke it, too.


\section{Regenerating the \themedgnome\ wrapper modules}
\label{sec:regeneration}


\begin{enumerate}
  \item
    Change to the directory where you have unpacked the
    \themedgnome\ package.
  \item
    Change to the subdirectory \code{theme-d-gen} and run command
    \code{make}.
  \item
    Change to the subdirectory \code{gnome/wrap} and give
    command \code{./generate.sh}.
  \item
    Change to the  subdirectory \code{gnome} and run command
    \code{make}.
\end{enumerate}


\section{Documentation}

Install the source code of \guilegnome\ and build
it. GTK Scheme documentation is found in file
\code{gtk/doc/gtk/guile-gnome-gtk.info}.
Programming with \themedgnome\ resembles closely programming with
\guilegnome\ and Scheme. See also \myurl{http://www.gtk.org/} for the
documentation of the GTK C library.


\section{Features different in \themedgnome\ and \guilegnome}


\subsection{Making instances of classes}


In \themedgnome\ instances of classes are created by creator
procedures. Creator procedures are named \code{xxx-create} or
\code{xxx-create-yyy} and they correspond to GTK constructor
procedures \code{xxx-new} and \code{xxx-new-yyy}. The GTK style
constructor procedures are supported by \themedgnome, too. The
difference between creators and GTK style constructor procedures is
that the result type of a creator is the class to be created. GTK
constructor procedures often have result type \code{<gtk-widget>} for
the subclasses of \code{<gtk-widget>}.


\subsection{Different procedures}


The following \themedgnome\ procedures take \themed\ procedures as
callbacks:
\begin{itemize}
\item \code{gtype-instance-signal-connect}
\item \code{connect}
\item \code{gtype-instance-signal-emit}
\item \code{emit}
\item \code{gtk-action-group-add-actions}
\item \code{add-actions}
\item \code{gtk-action-group-add-toggle-actions}
\item \code{add-toggle-actions}
\item \code{gtk-action-group-add-radio-actions}
\item \code{add-radio-actions}
\end{itemize}


As procedure purities are not specified by the \guilegnome\ library we
set the purity of all \themedgnome\ wrapper functions to
\code{nonpure}.


\subsubsection{\code{gtk-text-buffer-create-tag}}


\startprocsyntax
\begin{codeblock}
(gtk-text-buffer-create-tag textbuffer str args...)
\end{codeblock}


\startprocarguments
\begin{arglist}
  \argument{textbuffer}{\code{<gtk-text-buffer>}}{A text buffer}
  \argument{str}{\code{<string>}}{The name of the new tag}
  \argument{args}{a list}{The properties of the new tag}
\end{arglist}


\procresult{New tag}%
{\builtin{<gtk-text-tag>}}


\procpurity{nonpure}


This procedure is a method of generic procedure \code{create-tag}.


\subsubsection{\code{gtk-entry-new}}


\startprocsyntax
\begin{codeblock}
(gtk-entry-new)
\end{codeblock}


\noprocarguments


\procresult{New \code{<gtk-entry>} widget}%
{\builtin{<gtk-widget>}}


\procpurity{nonpure}


\subsubsection{\code{gtk-entry-create}}


\startprocsyntax
\begin{codeblock}
(gtk-entry-create)
\end{codeblock}


\noprocarguments


\procresult{New \code{<gtk-entry>} widget}%
{\builtin{<gtk-entry>}}


\procpurity{nonpure}


\subsubsection{\code{gtk-tree-or-list-store-set}}


Could not find documentation for this procedure.


\subsubsection{\code{gtk-tree-selection-get-selected}}


\startprocsyntax
\begin{codeblock}
(gtk-tree-selection-get-selected sel)
\end{codeblock}


\startprocarguments
\begin{arglist}
  \argument{sel}{\code{<gtk-tree-selection>}}{A tree selection}
\end{arglist}


\procresult{A pair of the tree model and tree iterator}%
{\code{(:pair <object> <gtk-tree-iter>)}}


\procpurity{nonpure}


This procedure is a method of generic procedure \code{get-selected}.


\subsubsection{\code{gtk-dialog-create}}


\startprocsyntax
\begin{codeblock}
(gtk-dialog-create \procarg{str-title} \procarg{modal?}
  \procarg{destroy-with-parent?})
\end{codeblock}


\startprocarguments
\begin{arglist}
  \argument{str-title}{\code{<string>}}{The title of the dialog}
  \argument{modal?}{\code{<boolean>}}{The modality of the dialog}
  \argument{destroy-with-parent?}{\code{<boolean>}}%
           {Destroy the dialog with its parent}
\end{arglist}


\procresult{A new dialog}%
{\code{<gtk-dialog>}}


\procpurity{nonpure}


\subsubsection{\code{gtk-dialog-new}}


\startprocsyntax
\begin{codeblock}
(gtk-dialog-new \procarg{str-title} \procarg{modal?}
  \procarg{destroy-with-parent?})
\end{codeblock}


\startprocarguments
\begin{arglist}
  \argument{str-title}{\code{<string>}}{The title of the dialog}
  \argument{modal?}{\code{<boolean>}}{The modality of the dialog}
  \argument{destroy-with-parent?}{\code{<boolean>}}%
           {Destroy the dialog with its parent}
\end{arglist}


\procresult{A new dialog}%
{\code{<gtk-widget>}}


\procpurity{nonpure}


\subsubsection{\code{gtk-frame-create-alt}}


\startprocsyntax
\begin{codeblock}
(gtk-frame-create-alt \procarg{str-label} \procarg{s-shadow-type}
  \procarg{i-width-request} \procarg{i-height-request})
\end{codeblock}


\startprocarguments
\begin{arglist}
  \argument{str-label}{\code{<string>}}{The label of the frame}
  \argument{s-shadow-type}{\code{<symbol>}}{The shadow type of the frame}
  \argument{i-width-request}{\code{<integer>}}%
           {The requested width of the frame}
  \argument{i-height-request}{\code{<integer>}}%
           {The requested height of the frame}
\end{arglist}


\procresult{A new frame}%
{\code{<gtk-frame>}}


\procpurity{nonpure}


The shadow type is one of the following symbols:
\begin{itemize}
\item \code{none}
\item \code{in}
\item \code{out}
\item \code{etched-in}
\item \code{etched-out}
\end{itemize}


\subsubsection{\code{gtk-frame-new-alt}}


\startprocsyntax
\begin{codeblock}
(gtk-frame-new-alt \procarg{str-label} \procarg{s-shadow-type}
  \procarg{i-width-request} \procarg{i-height-request})
\end{codeblock}


\startprocarguments
\begin{arglist}
  \argument{str-label}{\code{<string>}}{The label of the frame}
  \argument{s-shadow-type}{\code{<symbol>}}{The shadow type of the frame}
  \argument{i-width-request}{\code{<integer>}}%
           {The requested width of the frame}
  \argument{i-height-request}{\code{<integer>}}%
           {The requested height of the frame}
\end{arglist}


\procresult{A new frame}%
{\code{<gtk-widget>}}


\procpurity{nonpure}


\section{Widget classes that have been checked}


\begin{itemize}
  \item \code{<gtk-action-group>}
  \item \code{<gtk-box>}
  \item \code{<gtk-button>}
  \item \code{<gtk-button-box>} 
  \item \code{<gtk-cell-renderer-toggle>}
  \item \code{<gtk-cell-renderer-text>}
  \item \code{<gtk-check-button>}
  \item \code{<gtk-combo-box>}
  \item \code{<gtk-container>}
  \item \code{<gtk-dialog>}
  \item \code{<gtk-entry>}
  \item \code{<gtk-frame>}
  \item \code{<gtk-hbox>}
  \item \code{<gtk-hbutton-box>}
  \item \code{<gtk-hpaned>}
  \item \code{<gtk-image>}
  \item \code{<gtk-label>}
  \item \code{<gtk-list-store>}
  \item \code{<gtk-menubar>}
  \item \code{<gtk-message-dialog>}
  \item \code{<gtk-paned>}
  \item \code{<gtk-scrolled-window>}
  \item \code{<gtk-size-group>}
  \item \code{<gtk-statusbar>}
  \item \code{<gtk-table>}
  \item \code{<gtk-text-buffer>}
  \item \code{<gtk-text-iter>}
  \item \code{<gtk-text-view>}
  \item \code{<gtk-toolbar>}
  \item \code{<gtk-tree-selection>}
  \item \code{<gtk-tree-store>}
  \item \code{<gtk-tree-view>}
  \item \code{<gtk-tree-view-column>}
  \item \code{<gtk-ui-manager>}
  \item \code{<gtk-vbox>}
  \item \code{<gtk-vbutton-box>}
  \item \code{<gtk-vpaned>}
  \item \code{<gtk-widget>}
  \item \code{<gtk-window>}
\end{itemize}


\end{document}
