#!/bin/sh
# -*-scheme-*-
exec guile-gnome-alt-2 -e __main -s $0 "$@"
!#
     
;; Copyright (C) 2019 Tommi Höynälänmaa
;; Distributed under GNU Lesser General Public License version 3,
;; see file doc/LGPL-3.

(import (except (rnrs) assert map))
(import (theme-d runtime runtime-theme-d-environment))
(import (th-scheme-utilities parse-command-line))
(import (th-scheme-utilities stdutils))
(import (th-scheme-utilities hrecord))

(define gl-custom-files0 (getenv "THEME_D_CUSTOM_CODE"))

(define gl-custom-files (if (string? gl-custom-files0)
			    (split-string gl-custom-files0 #\:)
			    '()))

(for-each load-compiled gl-custom-files)

(define (__main args)
  (let* ((verbose-errors? #t)
	 (backtrace? #f)
	 (pretty-backtrace? #f)
	 (argd1 (make-hrecord <argument-descriptor> "no-verbose-errors" #f
			      (lambda () (set! gl-verbose-errors? #f))))
	 (argd2 (make-hrecord <argument-descriptor> "backtrace" #f
			      (lambda () (set! gl-backtrace? #t))))
	 (argd3 (make-hrecord <argument-descriptor> "pretty-backtrace" #f
			      (lambda () (set! gl-pretty-backtrace? #t))))
	 (args-without-cmd (cdr args))
	 (arg-descs (list argd1 argd2 argd3))
	 (proper-args '())
	 (handle-proper-args (lambda (proper-args1)
			       (set! proper-args proper-args1))))
    (parse-command-line args-without-cmd arg-descs handle-proper-args)
    (theme-set-command-line! proper-args)
    (guard (exc (else (my-error-exit exc)))
	   (load-compiled (car proper-args)))
    (if (not gl-script?)
	(begin
	  (set-verbose-errors! verbose-errors?)
	  (set-backtrace! backtrace?)
	  (set-pretty-backtrace! pretty-backtrace?)
	  (set-rte-exception-info! #t)
	  (main proper-args)))))
